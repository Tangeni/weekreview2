﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WeekReview2.Startup))]
namespace WeekReview2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
