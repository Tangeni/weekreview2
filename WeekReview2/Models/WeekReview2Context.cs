﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WeekReview2.Models
{
    public class WeekReview2Context : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public WeekReview2Context() : base("name=WeekReview2Context")
        {
        }

        public System.Data.Entity.DbSet<WeekReview2.Models.Recipe> Recipes { get; set; }
    }
}
