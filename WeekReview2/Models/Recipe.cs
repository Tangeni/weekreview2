﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WeekReview2.Models
{
    public class Recipe
    {
       
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Ingredient { get; set; }
        [Required]
        public string Comment { get; set; }
        public int Like { get; set; }
        [Required]
        public string TempComment { get; set; }
    }
    public class Recipes
    {
        public List<Recipe> Recipe { get; set; }
    }
}